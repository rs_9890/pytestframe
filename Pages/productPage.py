import logging


class ProductPage():
    def __init__(self, driver):
        self.driver = driver

        self.productPage_slctqty_button_xpath = "//select[@id='quantity']"
        self.productPage_add2cart_button_xpath = "//button[@title='Add to Shopping Cart']"
        self.productPage_add2cart_button1_xpath = "//input[@id='add-to-cart-button']"
        self.productPage_changeqty_drpdwn_xpath = "//select[@id='quantity']/option[%s]"
        self.productPage_total_qty_xpath = "//select[@id='quantity']/option"
        self.prodPage_home_icon_xpath = "//span[@class='nav-sprite nav-logo-base']"

    def click_quantity(self):
        self.driver.switch_to.window(self.driver.window_handles[1])
        self.driver.find_element_by_xpath(self.productPage_slctqty_button_xpath).click()
        self.driver.implicitly_wait(3)

    def add_to_cart(self):
        try:
            self.driver.find_element_by_xpath(self.productPage_add2cart_button_xpath).click()
        except:
            self.driver.find_element_by_xpath(self.productPage_add2cart_button1_xpath).click()
        self.driver.implicitly_wait(3)

    def change_quantity(self, qty):
        total_qty = self.driver.find_elements_by_xpath(self.productPage_total_qty_xpath)
        if 0 < qty < len(total_qty):
            self.driver.find_element_by_xpath(self.productPage_changeqty_drpdwn_xpath % qty).click()
            self.driver.implicitly_wait(3)
        else:
            logging.error("incorrect quantity entered")
            self.fail(msg="change qty failed")

    def goto_home(self):
        self.driver.find_element_by_xpath(self.prodPage_home_icon_xpath).click()
        self.driver.implicitly_wait(3)
