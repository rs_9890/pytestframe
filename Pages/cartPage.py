from Logger import log as logger
import logging

class CartPage():
    def __init__(self, driver):
        self.driver = driver

        self.cartPage_delete_xpath = "//input[contains(@aria-label,'%s') and contains(@name,'delete')]"
        self.cartPage_qty_drpdwn_xpath = "//input[contains(@aria-label,'%s') and contains(@name," \
                                         "'delete')]/../../..//span[@data-action='a-dropdown-button'] "
        self.cartPage_qty_drpdwn_value_xpath = "//a[@id='dropdown1_%s']"
        self.cartPage_ttl_qty_xpath="//a[contains(@id,'dropdown1')]"
        self.cartPage_proceed_checkout_button_xpath = "//input[@name='proceedToRetailCheckout']"

    def delete_item_cart(self, prodname):
        self.driver.find_element_by_xpath(self.cartPage_delete_xpath % prodname).click()
        self.driver.implicitly_wait(1)

    def change_qty_cart(self, prodname, qty):
        self.driver.find_element_by_xpath(self.cartPage_qty_drpdwn_xpath % prodname).click()
        total_qty = self.driver.find_elements_by_xpath(self.cartPage_ttl_qty_xpath)
        if 0 < qty < len(total_qty):
            self.driver.find_element_by_xpath(self.cartPage_qty_drpdwn_value_xpath % qty).click()
        else:
            logging.error("incorrect quantity entered")
            self.fail(msg="change qty failed")
        self.driver.implicitly_wait(2)

    def goto_checkout(self):
        self.driver.find_element_by_xpath(self.cartPage_proceed_checkout_button_xpath).click()
        self.driver.implicitly_wait(2)

