import logging
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
import time


class SearchPage():
    def __init__(self, driver):
        self.driver = driver
        self.searchPage_finditem_vlist_xpath = "//span[@class='celwidget slot=SEARCH_RESULTS template=SEARCH_RESULTS widgetId=search-results index=%s']//img[@class='s-image']"
        self.searchPage_select_item_xpath = "//li[@id='result_%s']//img[@class='s-access-image cfMarker']"
        self.prod_searchTextBox_xpath = "//input[@id='twotabsearchtextbox']"
        self.prod_searchIcon_xpath = "//div[@class='nav-search-submit nav-sprite']//input[@class='nav-input']"

    def search(self, searchitem):
        self.driver.find_element_by_xpath(self.prod_searchTextBox_xpath).send_keys(searchitem)
        self.driver.find_element_by_xpath(self.prod_searchIcon_xpath).click()
        self.driver.implicitly_wait(3)

    def select_nth_item_listpage(self, number):
        actions = ActionChains(self.driver)
        if number >= 1:
            try:
                ele = self.driver.find_element_by_xpath(self.searchPage_select_item_xpath % str(number - 1))
                actions.move_to_element(ele).perform()
                self.driver.find_element_by_xpath(self.searchPage_select_item_xpath % str(number - 1)).click()
            except:
                ele = self.driver.find_element_by_xpath(self.searchPage_finditem_vlist_xpath % str(number - 1))
                actions.move_to_element(ele).perform()
                self.driver.find_element_by_xpath(self.searchPage_finditem_vlist_xpath % str(number - 1)).click()

        else:
            logging.warning("incorrect item number")
