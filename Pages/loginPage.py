

class LoginPage():
    def __init__(self, driver):
        self.driver = driver
        self.loginPage_email_phone_textbox_xpath = "//input[@id='ap_email']"
        self.loginPage_continue_button_xpath = "//input[@id='continue']"
        self.loginPage_password_textbox_xpath = "//input[@id='ap_password']"
        self.loginPage_login_button_xpath = "//input[@id='signInSubmit']"

    def enter_email_phone(self, email_phone):
        self.driver.find_element_by_xpath(self.loginPage_email_phone_textbox_xpath).clear()
        self.driver.find_element_by_xpath(self.loginPage_email_phone_textbox_xpath).send_keys(email_phone)
        self.driver.find_element_by_xpath(self.loginPage_continue_button_xpath).click()

    def enter_password(self, password):
        self.driver.find_element_by_xpath(self.loginPage_password_textbox_xpath).clear()
        self.driver.find_element_by_xpath(self.loginPage_password_textbox_xpath).send_keys(password)

    def click_login(self):
        self.driver.find_element_by_xpath(self.loginPage_login_button_xpath).click()
