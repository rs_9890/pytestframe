from Logger import log as logger


class HomePage():
    def __init__(self, driver):
        self.driver = driver

        self.homePage_signInButton_xpath = "//*[@id='nav-link-accountList']/span[1]"
        self.homePage_hamburgerIcon_xpath = "//i[@class='hm-icon nav-sprite']"
        self.homePage_category_hamburger_xpath = "//div[contains(text(),'%s')]"
        self.homePage_category_style_hamburger_xpath = "//div[contains(text(),'%s')]"
        self.homePage_searchTextBox_xpath = "//input[@id='twotabsearchtextbox']"
        self.homePage_searchIcon_xpath = "//div[@class='nav-search-submit nav-sprite']//input[@class='nav-input']"
        self.homePage_goto_cart_xpath = "//span[@id='nav-cart-count']"

    def goto_loginpage(self):
        self.driver.find_element_by_xpath(self.homePage_signInButton_xpath).click()

    def search_from_home(self, searchitem):
        self.driver.find_element_by_xpath(self.homePage_searchTextBox_xpath).send_keys(searchitem)
        self.driver.find_element_by_xpath(self.homePage_searchIcon_xpath).click()

    def goto_cart(self):
        self.driver.find_element_by_xpath(self.homePage_goto_cart_xpath).click()

    def select_category_item_hamburger(self, category, styletype):
        self.driver.find_element_by_xpath(self.homePage_hamburgerIcon_xpath).click()
        self.driver.find_element_by_xpath(self.homePage_category_hamburger_xpath % category).click()
        self.driver.find_element_by_xpath(self.homePage_category_style_hamburger_xpath % styletype).click()
