import pytest
from Pages.loginPage import LoginPage
from Pages.homePage import HomePage
from Pages.searchPage import SearchPage
from Pages.productPage import ProductPage
from Pages.cartPage import CartPage
from Utils import utils as utils


@pytest.mark.usefixtures("test_setup")
class TestAssign():

    def test_home_page_title(self):
        self.driver.get(utils.URL)
        assert self.driver.title == "Online Shopping site in India: Shop Online for Mobiles, Books, Watches, Shoes and More - Amazon.in"

    def test_login(self):
        driver = self.driver
        homepage = HomePage(driver)
        homepage.goto_loginpage()
        login = LoginPage(driver)
        login.enter_email_phone(utils.USERNAME)
        login.enter_password(utils.PASSWORD)
        login.click_login()

    def test_feature_testcase(self):
        driver = self.driver
        homepage = HomePage(driver)
        searchPage = SearchPage(driver)
        prodPage = ProductPage(driver)
        cartPage = CartPage(driver)
        homepage.select_category_item_hamburger(utils.Category, utils.styleType)
        searchPage.select_nth_item_listpage(utils.searchitem1_no)
        prodPage.add_to_cart()
        searchPage.search(utils.searchitem2)
        searchPage.select_nth_item_listpage(utils.searchitem2_no)
        prodPage.click_quantity()
        prodPage.change_quantity(utils.searchitem2_qty)
        prodPage.add_to_cart()
        prodPage.goto_home()
        homepage.goto_cart()
        cartPage.delete_item_cart(utils.searchitem1)
        cartPage.change_qty_cart(utils.searchitem2, utils.searchitem2_qty_changed)
        cartPage.goto_checkout()
